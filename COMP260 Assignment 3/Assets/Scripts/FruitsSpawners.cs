﻿using UnityEngine;
using System.Collections;

public class FruitSpawner : MonoBehaviour {

    public int nFruit = 10;
    public FruitControl fruitPrefab;

    public float xMin, yMin;
    public float width, height;

    public float minFruitPeriod, maxFruitPeriod;

    private float clock, spawnDelay = 3f;

    // Use this for initialization
    void Start () {
       /* // create fruit
        for (int i = 0; i < nFruit; i++)
        {
            FruitControl fruit = Instantiate(fruitPrefab);

            // attach to this object in the hierarchy
            fruit.transform.parent = transform;
            // give the bee a name and number
            fruit.gameObject.name = "Apple " + i;

            // move the bee to a random position within 
            // the bounding rectangle
            float x = xMin + Random.value * width;
            float y = yMin + Random.value * height;
            fruit.transform.position = new Vector2(x, y);

        }*/
    }

    // Update is called once per frame
    void Update() {

        if (clock > 0)
        {
            clock -= Time.deltaTime;
        } else
        {
            // calcul new spawn delay
            spawnDelay = Mathf.Lerp(minFruitPeriod, maxFruitPeriod,
                Random.value);
            clock = spawnDelay;
            spawnFruit();
        }

    }
	    void spawnFruit() {
            FruitControl fruit = Instantiate(fruitPrefab);

            // attach to this object in the hierarchy
            fruit.transform.parent = transform;
            // give the bee a name and number
            fruit.gameObject.name = "Apple " + nFruit;
            nFruit++;

            // move the bee to a random position within 
            // the bounding rectangle
            float x = xMin + Random.value * width;
            float y = yMin + Random.value * height;
            fruit.transform.position = new Vector2(x, y);

        }
	}

