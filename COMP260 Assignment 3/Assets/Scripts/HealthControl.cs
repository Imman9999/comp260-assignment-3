﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthControl : MonoBehaviour {

	public Text text;
	public Text sText;
	public Text gameOver;
	public float setHealth;
	public static float health;
	public int setScore;
	public static int score;

	// Use this for initialization
	void Start () {
		health = setHealth;
		score = setScore;
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "HEALTH: " + health + "%";
		if (health > 100.0f) {
			health = 100.0f;
		} else if (health < 0.0f) {
			health = 0.0f;
			gameOver.text = "GAME OVER";
		}

		sText.text = "SCORE: " + score;
	}

	public static void healthDecrease(float damage){
		if (health >= 0.0f) {
			health = health - damage;
		}
	}

	public static void healthIncrease(float heal){
		if (health <= 100.0f) {
			health = health + heal;
		}
	}

	public static void scoreIncrease(int points){
		score = score + points;
	}
}
