﻿using UnityEngine;
using System.Collections;


public class AudioControl : MonoBehaviour {
    public AudioSource audioSrc;
    
    // Use this for initialization
	void Start () {
	
	}
    void Awake()
    {
			audioSrc.ignoreListenerVolume = true;
			audioSrc.Play ();
    }
    // Update is called once per frame
    void Update () {
		if (HealthControl.health <= 0) {
			audioSrc.Pause ();
		}
	}
}
