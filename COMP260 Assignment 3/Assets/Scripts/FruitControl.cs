﻿using UnityEngine;
using System.Collections;

// Play an audio clip a collision occurs
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody2D))]
public class FruitControl : MonoBehaviour {
    public AudioClip PlayerCollideClip;
    public AudioClip GroundCollideClip;
    private AudioSource audio;
    public LayerMask playerLayer;
    public LayerMask ground;
    private bool isGone = false;
    public Rigidbody2D rigidbody;
    
    private float clockRipe, RipeLimit = 1f;
    private Vector3 limitSizeRipe = new Vector3(1.4F, 1.4F, 1.4F);
    private Vector3 growthRate = new Vector3(0.25f, 0.25f, 0.25f);
    // Physics.gravity
    private Vector3 forceFall = new Vector3(0, -1.0F, 0);

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }

    void OnDestroy() {
        Debug.Log("Play audio source");
    }
        void OnCollisionEnter2D(Collision2D collision) {

        if (playerLayer.Contains(collision.gameObject)) {
			
			HealthControl.healthIncrease (20.0f);

			HealthControl.scoreIncrease (1);

            audio.PlayOneShot(PlayerCollideClip);
            SpriteRenderer _renderer = gameObject.GetComponent<SpriteRenderer>();
            _renderer.enabled = false;
            Destroy(gameObject, PlayerCollideClip.length); //waits until audio is played
         } 

        if(ground.Contains(collision.gameObject)) {
            audio.PlayOneShot(GroundCollideClip);
            Destroy(gameObject, GroundCollideClip.length);
        }
    }
		
    void Update () {
	
	}

    void FixedUpdate () {
        if (clockRipe > 0) {
            clockRipe -= Time.deltaTime;
        }
        // if the apple has not reach the limit, make it ripe
        else if (transform.localScale.magnitude <= limitSizeRipe.magnitude) {
            clockRipe = RipeLimit;
            ripeFruit();
		} else {
            // check if the size of the fruit has reached the limit, then the fruit can fall
            if (transform.localScale.magnitude >= limitSizeRipe.magnitude) {
                fallFruit();
            }
        }
    }

    // this function make the fruit falls
    void fallFruit() {
        // rigidbody.AddForce(Physics.gravity * rigidbody.mass);
        rigidbody.AddForce(forceFall * rigidbody.mass);
    }

    // this function make the fruit ripes

    void ripeFruit() {
        // It is what make the fruit grow over time
        transform.localScale += growthRate;
    }


}
