﻿using UnityEngine;
using System.Collections;

public class enemyAim : MonoBehaviour {
	public Transform Target;
	public shootMove projectile;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
			transform.right = Target.position - transform.position;
	}

	void instantiateProjectile(){
		shootMove shoot = (shootMove)Instantiate(projectile, gameObject.transform.position, gameObject.transform.rotation);
	}

	void OnCollisionEnter2D (Collision2D collision){
		Debug.Log ("HI");
	}
}
