﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour {

	private Vector3 forceFall = new Vector3(0, -1.0F, 0);

    public float speed = 600f;
	public Rigidbody2D rigidbody;
    public LayerMask boundary;

    void Start () {
		rigidbody = GetComponent<Rigidbody2D> ();
    }


    void FixedUpdate() {
		if (HealthControl.health > 0) {
			Vector2 pos = rigidbody.position;
			Vector2 vel = pos.normalized * speed;

			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			Vector2 v = rigidbody.velocity;

			vel = direction * speed;

			rigidbody.velocity = vel * Time.fixedDeltaTime;
		
		} else if(HealthControl.health <= 0.0f){
			Destroy (gameObject);
		}

	}
}
